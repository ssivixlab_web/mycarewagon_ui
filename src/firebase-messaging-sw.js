importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js');

var firebaseConfig = {
  apiKey: 'AIzaSyCZupm5IuERS8vu0ABSxsRCCvFlebVJT10',
  authDomain: 'mycarewagon-provider.firebaseapp.com',
  databaseURL: 'https://mycarewagon-provider.firebaseio.com',
  projectId: 'mycarewagon-provider',
  storageBucket: 'mycarewagon-provider.appspot.com',
  messagingSenderId: '59044392137',
  appId: '1:59044392137:web:4529fb4735e7fd0efa7118',
  measurementId: 'G-YJ9RT90SH4'
};
firebase.initializeApp(firebaseConfig);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// messaging.onMessage((payload) => {

// });
messaging.setBackgroundMessageHandler(function(payload) {
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});