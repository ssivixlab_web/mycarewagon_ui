// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endPointUrl: 'http://18.222.133.72:3000/',
  firebaseConfig : {
    apiKey: 'AIzaSyCZupm5IuERS8vu0ABSxsRCCvFlebVJT10',
    authDomain: 'mycarewagon-provider.firebaseapp.com',
    databaseURL: 'https://mycarewagon-provider.firebaseio.com',
    projectId: 'mycarewagon-provider',
    storageBucket: 'mycarewagon-provider.appspot.com',
    messagingSenderId: '59044392137',
    appId: '1:59044392137:web:4529fb4735e7fd0efa7118',
    measurementId: 'G-YJ9RT90SH4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
