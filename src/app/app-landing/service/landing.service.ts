import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { AppHttpApiService } from 'src/app/app-common/utility/http-api.service';
import { API_URL, SESSION_STORAGE_KEY } from 'src/app/app-common/contants';
import { ILoginPayload, ILoginResponse, IRegistrationPayload, IVerifyForgotPaswordRes } from './landing';
import { HttpHeaders } from '@angular/common/http';
import { MessagingService } from 'src/app/app-secured/common/appService/messaging.service';
import { map, catchError } from 'rxjs/operators';
import { AppAuthenticationService } from 'src/app/app-common/utility/authentication.service';

@Injectable({
    providedIn: 'root',
})
export class AppLandingService {
    constructor(private httpApi: AppHttpApiService,
                private messagingService: MessagingService) {
                    this.messagingService.getFcmToken();
    }

    private updatePayloadforNotification(payload: any) {
        payload.userFcmToken = this.messagingService.getFcmToken();
        return payload;
    }

    public setSession(user) {
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.TOKEN, user.session);
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(user.data));
    }

    public checkUserLogin(payload: ILoginPayload): Observable<boolean> {
        payload = this.updatePayloadforNotification(payload);
        return this.httpApi.postData(API_URL.LOGIN, payload)
        .pipe(
            map((res: ILoginResponse) => {
                let isVerified: boolean = false;
                if (res && res.data && res.status && res.session) {
                    this.setSession(res);
                    isVerified = true;
                } else {
                    isVerified = false;
                }
                return isVerified;
            }),
            catchError((err) => {
                throw  err.error;
            })
        );
    }

    public registerUser(payload: IRegistrationPayload): Observable<boolean> {
        payload = this.updatePayloadforNotification(payload);
        return new Observable((observer) => {
            let tempRegister: boolean = false;
            return this.httpApi.postData(API_URL.REGISTER, payload)
            .subscribe({
                next: (res) => {
                    if (res && res.status) {
                        tempRegister = true;
                    } else {
                        tempRegister = false;
                    }
                },
                error: (err: any) =>  {
                    observer.error(err.error);
                    tempRegister = false;
                },
                complete: () => {
                    observer.next(tempRegister);
                    observer.complete();
                }
            });
        });
    }
    // TODO: set session return boolean
    public verifyOTP(payload: IRegistrationPayload, file?) {
        const formData = new FormData();
        if (file) {
            formData.append('image', file, file.name);
        }
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        formData.append('data', JSON.stringify(payload));
        return this.httpApi.postData(API_URL.OTP_VERIFY, formData, HttpUploadOptions.headers);
    }

    verifyForgotPasswordUser(payload): Observable<IVerifyForgotPaswordRes> {
        return this.httpApi.postData(API_URL.FORGOT_PASWORD, payload);
    }

    resetPassword(payload): Observable<IVerifyForgotPaswordRes> {
        return this.httpApi.postData(API_URL.RESET_PASWORD, payload);
    }
}
