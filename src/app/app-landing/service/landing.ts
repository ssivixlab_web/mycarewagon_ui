export interface ILoginPayload {
    email: string;
    password: string;
    userFcmToken?: string;
    OSType?: string;
}

export interface ILoginResponse {
    data: IUserData;
    session: string;
    status: boolean;
}

export interface IUserData {
    companyName: string;
    companyAddress: string;
    phoneNumber: string;
    email: string;
    logo: string | null;
    country: string | null;
    city: string | null;
    _id: string;
}

export interface IRegistrationPayload {
    companyName: string;
    email: string;
    countryCode: string;
    phoneNumber: string;
    password: string;
    userFcmToken: string;
    OSType: null;
    OTP: number;
    companyAddress: string;
    driver: {
        driverName: string;
        phoneNumber: string;
        countryCode: string;
        address: string;
    };
    vehicle: {
        vehicleNumber: string;
        vehicleType: string;
    };
}

export interface IVerifyForgotPaswordRes {
    status: boolean;
    data: any;
    error_code: number;
    error_message: string;
}
