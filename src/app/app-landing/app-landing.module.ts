import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppLandingRoutingModule } from './app-landing-routing.module';
import { AppLandingComponent } from './app-landing.component';
import { AppLoginComponent } from './login/login.component';
import { AppRegisterComponent } from './register/register.component';
import { AppMaterialModule } from '../app-common/app-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppOtpVerifyComponent } from './otp-verify/otp-verify.componet';
import { NgOtpInputModule } from 'ng-otp-input';
import { AppAuthenticationService } from '../app-common/utility/authentication.service';
import { AppHttpApiService } from '../app-common/utility/http-api.service';
import { AppTermsConditionComponent } from '../app-secured/terms-condition/terms-condition.component';
import { AppCommonModule } from '../app-common/app-common.module';
import { AppForgotPasswordComponent } from './forgot-password/forgot-password.component';

@NgModule({
  declarations: [
    AppLandingComponent,
    AppLoginComponent,
    AppRegisterComponent,
    AppOtpVerifyComponent,
    AppForgotPasswordComponent
  ],
  entryComponents: [
    AppOtpVerifyComponent,
    AppTermsConditionComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    AppLandingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgOtpInputModule,
    AppCommonModule
  ],
  providers: [
    AppAuthenticationService,
    AppHttpApiService
  ]
})
export class AppLandingModule { }
