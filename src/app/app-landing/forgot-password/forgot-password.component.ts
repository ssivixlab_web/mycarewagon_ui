import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppLandingService } from '../service/landing.service';
import { IVerifyForgotPaswordRes } from '../service/landing';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class AppForgotPasswordComponent implements OnInit {
  forgotpasswordForm: FormGroup;
  formValueSnap;
  errors: string = '';
  isUserEmailVerified: boolean = false;
  showOtpComponent = true;
    invalidOtp: string = '';
    config = {
        allowNumbersOnly: true,
        length: 4,
        isPasswordInput: false,
        disableAutoFocus: true,
        inputStyles: {
            width: '50px',
            height: '35px',
            border: '0px',
            background: 'transparent',
            'border-bottom': '1px solid #c5c5c5',
            'border-radius': '0px',
            'font-size': 'inherit'
        },
        containerClass: 'pt-2'
    };
  constructor(private router: Router, private snackBar: MatSnackBar,
              private appService: AppLandingService) { }

  ngOnInit() {
    this.forgotpasswordForm = new FormGroup({
      email: new FormControl('', Validators.required),
    });
  }

  onOtpChange(otp) {
    this.forgotpasswordForm.get('OTP').setValue(otp);
  }

  onConfigChange() {
    this.showOtpComponent = false;
    this.forgotpasswordForm.get('OTP').setValue(null);
    setTimeout(() => {
        this.showOtpComponent = true;
    }, 0);
  }

  addNewFormControl() {
    this.forgotpasswordForm.addControl('password', new FormControl('', Validators.required));
    this.forgotpasswordForm.addControl('OTP', new FormControl('', Validators.required));
    const formObj = {
      email: this.forgotpasswordForm.value.email
    };
    this.forgotpasswordForm.reset();
    this.forgotpasswordForm.markAsUntouched();
    this.forgotpasswordForm.patchValue(formObj);
    this.forgotpasswordForm.get('email').disable();
  }

  verifyUserEmail(payload): void {
    this.errors = '';
    this.appService.verifyForgotPasswordUser(payload).subscribe((res: IVerifyForgotPaswordRes) => {
      if (res.status) {
        this.isUserEmailVerified = true;
        this.addNewFormControl();
      } else if (!res.status && res.data !== '') {
        this.errors = res.data;
      } else {
        this.errors = '';
      }
    }, (err) => {
     if (err.error && err.error.error_message) {
      this.errors = err.error.error_message;
     }
    });
  }

  resetPassword() {
    this.appService.resetPassword(this.forgotpasswordForm.getRawValue()).subscribe((res) => {
      if (res.status) {
        this.errors = '';
        this.openSnackBar(`${res.data}`, 'CLOSE');
      } else if (!res.status && res.data !== '') {
        this.errors = res.data;
      } else {
        this.errors = '';
      }
    }, (err) => {
      if (err.error && err.error.error_message) {
        this.errors = err.error.error_message;
       }
    });
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      horizontalPosition: 'end',
      verticalPosition: 'bottom',
      duration: 3000
    });

    snackBarRef.onAction().subscribe(() => {
      this.router.navigateByUrl('login');
    });
  }

}
