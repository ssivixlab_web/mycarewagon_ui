import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLandingComponent } from './app-landing.component';
import { AppLoginComponent } from './login/login.component';
import { AppRegisterComponent } from './register/register.component';
import { AppForgotPasswordComponent } from './forgot-password/forgot-password.component';

export const appLandingRoutes: Routes = [{
  path: '',
  component: AppLandingComponent,
  children: [{
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: AppLoginComponent,
  },
  {
    path: 'register',
    component: AppRegisterComponent
  },
  {
    path: 'forgot-password',
    component: AppForgotPasswordComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(appLandingRoutes)],
  exports: [RouterModule]
})
export class AppLandingRoutingModule { }
