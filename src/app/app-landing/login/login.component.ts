import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILoginPayload } from '../service/landing';
import { Router } from '@angular/router';
import { AppLandingService } from '../service/landing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AppLoginComponent implements OnInit {
  loginForm: FormGroup;
  errors: string = '';
  constructor(private appService: AppLandingService,
              private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submit(payload: ILoginPayload): void {
    this.errors = '';
    this.appService.checkUserLogin(payload)
      .subscribe({
        next: (res: boolean) => {
          if (res) {
            this.router.navigateByUrl('auth');
          }
        },
        error: (err: any) =>  {
          this.errors = err.error_message;
        },
        complete: () => {

        }
    });
  }

}
