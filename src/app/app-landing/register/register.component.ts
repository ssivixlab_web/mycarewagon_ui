import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppOtpVerifyComponent } from '../otp-verify/otp-verify.componet';
import * as _ from 'lodash';
import { AppLandingService } from '../service/landing.service';
import { VEHICLE } from 'src/app/app-common/contants';
import { AppTermsConditionComponent } from 'src/app/app-secured/terms-condition/terms-condition.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class AppRegisterComponent implements OnInit {
  registerForm: FormGroup;
  showdriver: boolean = false;
  showVehicle: boolean = false;
  dicon: string = 'add';
  aicon: string = 'add';
  vechiletypleList: any[];
  imgURL: any;
  files: any;
  errors: string = '';
  constructor(private fb: FormBuilder, private dialog: MatDialog,
              private appLandingService: AppLandingService) {
    this.registerForm = this.fb.group({
      companyName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      countryCode: new FormControl('+65', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
      companyAddress: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      driver: this.createDriver(),
      vehicle: this.createVehicle(),
      accept: new FormControl('', [Validators.required, Validators.pattern('true')]),
    });
   }

  ngOnInit() {
    this.vechiletypleList = VEHICLE;
  }

  onFileChanged(event: any) {
    this.files = event.target.files[0];

    if (this.files.length === 0) {
      return;
    }

    const mimeType = this.files.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.files);
    reader.onload = () => {
      this.imgURL = reader.result;
    };
  }

  removeImage() {
    this.imgURL = null;
    this.files = null;
  }

  createDriver(): FormGroup {
    return this.fb.group({
      driverName: new FormControl('', Validators.required),
      countryCode: new FormControl('+65', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required)
    });
  }

  createVehicle(): FormGroup {
    return this.fb.group({
      vehicleNumber: new FormControl('', Validators.required),
      vehicleType: new FormControl('', Validators.required)
    });
  }

  toogleDriver(): void {
    if (this.showdriver) {
      this.dicon = 'add';
    } else {
      this.dicon = 'remove';
    }
    this.showdriver = !this.showdriver;
  }

  toogleVehicle(): void {
    if (this.showVehicle) {
      this.aicon = 'add';
    } else {
      this.aicon = 'remove';
    }
    this.showVehicle = !this.showVehicle;
  }

  submit(payload) {
    this.errors = '';
    const newPayload = _.cloneDeep(payload);
    delete newPayload.accept;
    this.appLandingService.registerUser(newPayload).subscribe({
      next: (res: boolean) => {
        if (res) {
          this.verifyOTP(newPayload);
        }
      },
      error: (err: any) =>  {
        this.errors = err.error_message;
      },
      complete: () => {

      }
    });
  }

  verifyOTP(payload) {
    const dialogRef = this.dialog.open(AppOtpVerifyComponent, {
        width: '450px',
        data: {
          formData: payload,
          file: this.files
        }
      });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  showModalWindow() {
    this.dialog.open(AppTermsConditionComponent, {
      height: '400px',
      width: '600px',
    });
  }

}
