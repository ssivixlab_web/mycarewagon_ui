import { Component, OnInit } from '@angular/core';
import { AppAuthenticationService } from 'src/app/app-common/utility/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-landing',
  templateUrl: './app-landing.component.html',
  styleUrls: ['./app-landing.component.scss']
})
export class AppLandingComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
    if (AppAuthenticationService.isAuthenticated()) {
      this.router.navigateByUrl('auth');
    }
  }

}
