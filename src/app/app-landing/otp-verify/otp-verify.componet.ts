import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppLandingService } from '../service/landing.service';
import { IRegistrationPayload } from '../service/landing';
import { Router } from '@angular/router';

@Component({
    selector: 'app-otp-verify',
    templateUrl: 'otp-verify.component.html',
})
export class AppOtpVerifyComponent {
    otp: string;
    showOtpComponent = true;
    invalidOtp: string = '';
    config = {
        allowNumbersOnly: true,
        length: 4,
        isPasswordInput: false,
        disableAutoFocus: false,
        inputStyles: {
            width: '50px',
            height: '50px'
        },
        containerClass: 'pt-2'
    };
    constructor(
        private dialogRef: MatDialogRef<AppOtpVerifyComponent>,
        private appLandingService: AppLandingService,
        private router: Router,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    onOtpChange(otp) {
        this.otp = otp;
    }

    onConfigChange() {
        this.showOtpComponent = false;
        this.otp = null;
        setTimeout(() => {
            this.showOtpComponent = true;
        }, 0);
    }

    submitOTP() {
        this.invalidOtp = '';
        const formdata: IRegistrationPayload = this.data.formData;
        formdata.OTP = parseInt(this.otp, 10);

       // this.dialogRef.close();
        this.appLandingService.verifyOTP(formdata, this.data.file).subscribe({
            next: (response) => {
                if (response.status && response.saved) {
                    this.dialogRef.close('Success');
                    this.makeUserLogin(response);
                }
            },
            error: (err: any) =>  {
                this.invalidOtp = err.error_message;
            },
            complete: () => { }
        });
    }

    makeUserLogin(payload) {
        this.appLandingService.setSession(payload);
        this.router.navigateByUrl('auth');
    }

}
