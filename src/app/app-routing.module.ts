import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAuthGuardService as AuthGuard } from './app-common/utility/auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./app-landing/app-landing.module').then(module => module.AppLandingModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./app-secured/app-secured.module').then(module => module.AppSecuredModule),
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,
    {
      enableTracing: false, // <-- debugging purposes only
      useHash: true
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
