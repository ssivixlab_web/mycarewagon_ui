import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppDashboardComponent } from './dashboard/dashboard.component';
import { AppLayoutComponent } from './layout/layout.component';
import { AppHistoryComponent } from './history/history.component';
import { AppDriverComponent } from './driver/driver.component';
import { AppVehicleComponent } from './vehicle/vehicle.component';
import { AppPolicyComponent } from './policy/policy.component';
import { AppProfileComponent } from './profile/profile.component';
import { AppScheduleRequestComponent } from './schedule-request/schedule-request.component';
import { AppNotificationComponent } from './notification/notification.component';
import { AppTermsConditionComponent } from './terms-condition/terms-condition.component';
import { AppSettingsComponent } from './settings/settings.component';

export const ComponentsRoutes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        component: AppDashboardComponent,
        data: {
          title: 'Dashboard',
          urls: []
        }
      },
      {
        path: 'history',
        component: AppHistoryComponent,
        data: {
          title: 'History',
          urls: []
        }
      },
      {
        path: 'driver',
        component: AppDriverComponent,
        data: {
          title: 'Driver Management',
          urls: []
        }
      },
      {
        path: 'vehicle',
        component: AppVehicleComponent,
        data: {
          title: 'Vehicle Management',
          urls: []
        }
      },
      {
        path: 'notification',
        component: AppNotificationComponent,
        data: {
          title: 'Notification',
          urls: []
        }
      },
      {
        path: 'request',
        component: AppScheduleRequestComponent,
        data: {
          title: 'Scheduled Request',
          urls: []
        }
      },
      {
        path: 'policy',
        component: AppPolicyComponent,
        data: {
          title: 'Privacy Policy',
          urls: []
        }
      },
      {
        path: 'terms',
        component: AppTermsConditionComponent,
        data: {
          title: 'Terms And Conditions',
          urls: []
        }
      },
      {
        path: 'profile',
        component: AppProfileComponent,
        data: {
          title: 'Profile',
          urls: []
        }
      },
      {
        path: 'settings',
        component: AppSettingsComponent,
        data: {
          title: 'Settings',
          urls: []
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(ComponentsRoutes)],
  exports: [RouterModule]
})
export class AppsecuredRoutingModule {}
