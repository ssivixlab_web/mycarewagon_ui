import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppsecuredRoutingModule } from './app-secured-routing.module';
import { AppDashboardComponent } from './dashboard/dashboard.component';
import { AppMaterialModule } from '../app-common/app-material.module';
import { AppLayoutComponent } from './layout/layout.component';
import { AppNavbarComponent } from './common/navbar/navbar.component';
import { AppFooterComponent } from './common/footer/footer.component';
import { AppService } from './common/appService/appService';
import { AppSidebarComponent } from './common/sidebar/sidebar.component';
import { AppSidebarService } from './common/sidebar/sidebar.service';
import { AppHistoryComponent } from './history/history.component';
import { AppDriverComponent } from './driver/driver.component';
import { AppVehicleComponent } from './vehicle/vehicle.component';
import { AppPolicyComponent } from './policy/policy.component';
import { AppviewRequestComponent } from './view-request/view-request.componet';
import { AppCreateEditDriverComponent } from './driver/create-edit-driver/create-edit-driver.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCreateEditVehicleComponent } from './vehicle/create-edit-vehicle/create-edit-vehicle.component';
import { AppProfileComponent } from './profile/profile.component';
import { AppbreadcrumbComponent } from './common/breadcrumb/breadcrumb.component';
import { AppScheduleRequestComponent } from './schedule-request/schedule-request.component';
import { AppAuthenticationService } from '../app-common/utility/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { AppProviderService } from './common/service/provider.service';
import { AppalertComponent } from './common/alert/alert.component';
import { AppViewTripDetailsComponent } from './history/view-trip-details/view-trip-details.component';
import { AppNotificationComponent } from './notification/notification.component';
import { AppPublishSubscribeService } from '../app-common/pub-sub/publish-subscribe.service';
import { AppCommonModule } from '../app-common/app-common.module';
import { AppSettingsComponent } from './settings/settings.component';
import { AppPushNotificationComponent } from './common/push-notification/pushNotification.component';
import { AppacceptRequestComponent } from './accept-request/accept-request.componet';

@NgModule({
  declarations: [
    AppLayoutComponent,
    AppbreadcrumbComponent,
    AppSidebarComponent,
    AppNavbarComponent,
    AppFooterComponent,
    AppDashboardComponent,
    AppScheduleRequestComponent,
    AppHistoryComponent,
    AppDriverComponent,
    AppVehicleComponent,
    AppPolicyComponent,
    AppProfileComponent,
    AppviewRequestComponent,
    AppCreateEditDriverComponent,
    AppCreateEditVehicleComponent,
    AppalertComponent,
    AppViewTripDetailsComponent,
    AppNotificationComponent,
    AppSettingsComponent,
    AppPushNotificationComponent,
    AppacceptRequestComponent
  ],
  entryComponents: [
    AppviewRequestComponent,
    AppCreateEditDriverComponent,
    AppCreateEditVehicleComponent,
    AppalertComponent,
    AppViewTripDetailsComponent,
    AppPushNotificationComponent,
    AppacceptRequestComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    AppsecuredRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppCommonModule
  ],
  providers: [
    AppService,
    AppPublishSubscribeService,
    AppSidebarService,
    AppAuthenticationService,
    AppProviderService,
  ]
})
export class AppSecuredModule { }
