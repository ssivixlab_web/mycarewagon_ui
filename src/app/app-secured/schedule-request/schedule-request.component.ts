import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { AppviewRequestComponent } from '../view-request/view-request.componet';
import { AppProviderService } from '../common/service/provider.service';
import { IScheduleTrip } from '../common/service/provider';

@Component({
  selector: 'app-schedule-request',
  templateUrl: './schedule-request.component.html',
  styleUrls: ['./schedule-request.component.scss']
})
export class AppScheduleRequestComponent implements OnInit {
  scheduleTrip$;

  constructor(public dialog: MatDialog, private appProviderService: AppProviderService) { }

  ngOnInit() {
    this.appProviderService.getScheduleTrip()
      .subscribe({
        next: (res: IScheduleTrip) => {
          this.scheduleTrip$ = res.data;
        },
        error: (err: any) =>  {
        },
        complete: () => {
        }
    });
  }

  openDialog(request): void {
    const dialogRef = this.dialog.open(AppviewRequestComponent, {
      width: '450px',
      data: request
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
