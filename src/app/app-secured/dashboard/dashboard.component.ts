import { Component, OnInit } from '@angular/core';
import { AppProviderService } from '../common/service/provider.service';
import { IUserDashBoard } from '../common/service/provider';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class AppDashboardComponent implements OnInit {
  details: any = {
    totalDriver: '-',
    totalRevenue: '-',
    totalVehicle: '-',
    requestCompleted: '-'
  };
  constructor(private appProviderService: AppProviderService) { }

  ngOnInit() {
    this.appProviderService.getUserDashboard()
      .subscribe({
        next: (res: IUserDashBoard) => {
          this.details = res.data;
        },
        error: (err: any) =>  {
        },
        complete: () => {
        }
    });
  }
}
