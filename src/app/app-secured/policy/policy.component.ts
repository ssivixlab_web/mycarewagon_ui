import { Component, OnInit } from '@angular/core';
import { STATIC_BASE_API_URL } from 'src/app/app-common/contants';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss']
})
export class AppPolicyComponent implements OnInit {
  ifWrapper;
  constructor() { }

  ngOnInit() {
    const url = STATIC_BASE_API_URL.PRIVACY;
    this.ifWrapper = `<iframe class="w-100 h-100" src="${url}" frameborder="0"></iframe>`;
  }

}
