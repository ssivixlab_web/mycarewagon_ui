import { Component, OnInit } from '@angular/core';
import { AppProviderService } from '../common/service/provider.service';
import { IBookingHistory, IAmbulanceBooking, InvoiceResponse } from '../common/service/provider';
import { BehaviorSubject } from 'rxjs';
import { MatBottomSheet, MatSnackBar } from '@angular/material';
import { AppViewTripDetailsComponent } from './view-trip-details/view-trip-details.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class AppHistoryComponent implements OnInit {
  completedTrips$ = new BehaviorSubject<IAmbulanceBooking[]>([]);
  displayedColumns: string[] = ['id', 'billRef', 'date',
  'name', 'amount', 'action'];
  page: number = 1;
  loadmoreflag: boolean = false;
  constructor(private appProviderService: AppProviderService,
              private bottomSheet: MatBottomSheet,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getCompletedTripList();
  }

  getCompletedTripList() {
    this.appProviderService.getCompletedTripList({page: this.page})
      .subscribe({
        next: (res: IBookingHistory) => {
          this.update(res.data);
        },
        error: (err: any) =>  {
        },
        complete: () => {
        }
    });
  }

  update(moreData: Array<IAmbulanceBooking>) {
    if (moreData.length < 10) {
      this.loadmoreflag = true;
    }
    const currentValue = this.completedTrips$.value;
    const updatedValue = currentValue.concat(...moreData);
    this.completedTrips$.next(updatedValue);
  }

  loadMore() {
    this.page = this.page + 1;
    this.getCompletedTripList();
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      horizontalPosition: 'end',
      verticalPosition: 'bottom',
      duration: 2000
    });

    snackBarRef.onAction().subscribe(() => {
    });
  }

  onContextMenuAction1(str, obj) {
    if (str === 'download') {
      const invoiceDetail = {
        bookingId : obj._id
      };
      this.appProviderService.getInvoice(invoiceDetail)
      .subscribe({
        next: (res: InvoiceResponse) => {
          if (res.status && res.data.status) {
            this.openSnackBar(res.data.data, 'CLOSE');
          }
        },
        error: (err: any) =>  {
        },
        complete: () => {
        }
      });
    } else {
      // View
      this.bottomSheet.open(AppViewTripDetailsComponent, {
        data: obj
      });
    }
  }

}
