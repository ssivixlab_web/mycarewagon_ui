import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { IAmbulanceBooking, TRIP_TYPE } from '../../common/service/provider';
import * as moment from 'moment';

@Component({
    selector: 'app-view-trip-details',
    templateUrl: 'view-trip-details.component.html',
  })
export class AppViewTripDetailsComponent implements OnInit {
    constructor(private bottomSheetRef: MatBottomSheetRef<AppViewTripDetailsComponent>,
                @Inject(MAT_BOTTOM_SHEET_DATA) public data: IAmbulanceBooking) {}

    ngOnInit() {
    }

    openLink(event: MouseEvent): void {
      this.bottomSheetRef.dismiss();
      event.preventDefault();
    }

    getTime(bookingTime): string {
      return moment(bookingTime).format('MMM Do, YYYY  h:mm a');
    }

    getUnit(unitNumber): string {
     return !unitNumber || unitNumber === '' ? 'NA' : unitNumber;
    }

    getEnumUIVal(key): string {
      return TRIP_TYPE[key];
    }

    getName() {
     return this.data.requestedBy.relativeDetails.length !== 0 ?
      (this.data.requestedBy.relativeDetails[0].firstName).concat(' ' + this.data.requestedBy.relativeDetails[0].lastName) : '';
    }
}
