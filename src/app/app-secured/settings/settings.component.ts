import { Component, OnInit } from '@angular/core';
import { MessagingService } from '../common/appService/messaging.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class AppSettingsComponent implements OnInit {
  permissionGranted: boolean = false;
  msg: string;
  constructor( private messagingService: MessagingService) {

  }

  ngOnInit() {
    const fcmToken = this.messagingService.getFcmToken();
    if (fcmToken !== '' && fcmToken !== undefined) {
      this.permissionGranted = true;
    }

    this.messagingService.token$.subscribe((res) => {
      if (res != null && res !== undefined) {
        this.permissionGranted = true;
      }
    });
  }

  test() {
    // this.messagingService.requestPermission();
    // const fcmToken = this.messagingService.getFcmToken();
    // if (fcmToken !== '' && fcmToken !== undefined) {
    //   this.permissionGranted = true;
    // } else {
    //   this.permissionGranted = false;
    //   this.msg = 'Please click allow to get Notified';
    // }
  }
}
