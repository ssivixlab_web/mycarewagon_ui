import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppProviderService } from '../common/service/provider.service';
import { IScheduleTrip } from '../common/service/provider';

@Component({
    selector: 'app-view-request.component',
    templateUrl: 'view-request.component.html',
    styleUrls: ['./view-request.component.scss']
})
export class AppviewRequestComponent {
    // tripStatus:string = "Start";
    constructor(public dialogRef: MatDialogRef<AppviewRequestComponent>,
                @Inject(MAT_DIALOG_DATA) public item: any, private appProviderService: AppProviderService) {
    }

    pickupStart(id): void {
        this.appProviderService.pickupTrip({bookingId: id})
        .subscribe({
            next: (res: IScheduleTrip) => {
                // this.tripStatus = "End";
                this.dialogRef.close(res);
            },
            error: (err: any) =>  {
            },
            complete: () => {
            }
        });
    }

    tripCompleted(id): void {
        this.appProviderService.tripCompleted({bookingId: id})
        .subscribe({
            next: (res: IScheduleTrip) => {
                // this.tripStatus = "End";
                this.dialogRef.close(res);
            },
            error: (err: any) =>  {
            },
            complete: () => {
            }
        });
    }

}
