import { Component, OnInit } from '@angular/core';
import { AppService } from '../common/appService/appService';
import { MessagingService } from '../common/appService/messaging.service';
import { MatSnackBar, MatSnackBarDismiss, MatDialog } from '@angular/material';
import { IPushNotificationResponse } from '../common/service/provider';
import { AppPushNotificationComponent } from '../common/push-notification/pushNotification.component';
import { AppacceptRequestComponent } from '../accept-request/accept-request.componet';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  constructor(private appService: AppService,
              private messagingService: MessagingService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {}

  getClasses() {
    const classes = {
      'pinned-sidebar': this.appService.getSidebarStat().isSidebarPinned,
      'toggeled-sidebar': this.appService.getSidebarStat().isSidebarToggeled
    };
    return classes;
  }

  toggleSidebar() {
    this.appService.toggleSidebar();
  }

  ngOnInit() {
    this.messagingService.getFcmToken();
    this.messagingService.receiveMessage();
    this.messagingService.currentMessage$.subscribe((notificationRes: IPushNotificationResponse) => {
      if (notificationRes !== null) {
        const snackBarRef = this.snackBar.openFromComponent(AppPushNotificationComponent, {
          data: notificationRes
        // duration: 2 * 1000,
        });
        snackBarRef.afterDismissed().subscribe((snackbar: MatSnackBarDismiss) => {
          if (snackbar.dismissedByAction) {
            this.openAcceptRequestModal(notificationRes);
          }
        });
      }
    });
  }

  openAcceptRequestModal(notificationRes) {
    const dialogRef = this.dialog.open(AppacceptRequestComponent, {
        width: '450px',
        data: notificationRes
      });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
