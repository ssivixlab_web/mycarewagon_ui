import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppProviderService } from '../../common/service/provider.service';
import { IDriverPayload, IDriverList } from '../../common/service/provider';
import { OPERATION_TYPE } from 'src/app/app-common/contants';

@Component({
    selector: 'app-create-edit-driver',
    templateUrl: './create-edit-driver.component.html',
    styleUrls: ['./create-edit-driver.component.scss']
  })
  export class AppCreateEditDriverComponent implements OnInit {
    driverForm: FormGroup;
    isAdd: boolean = true;
    constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<AppCreateEditDriverComponent>,
                private appProviderService: AppProviderService,
                @Inject(MAT_DIALOG_DATA) public driverEdit: IDriverList) {
        this.driverForm = this.fb.group({
            driverName: new FormControl('', Validators.required),
            countryCode: new FormControl('+65'),
            phoneNumber: new FormControl('', Validators.required),
            address: new FormControl('', Validators.required)
        });
    }

    addDriver(payload: IDriverPayload): void {
        const opType =  this.isAdd ? OPERATION_TYPE.ADD : OPERATION_TYPE.EDIT;
        this.appProviderService.driverManagement(opType, payload).subscribe({
            next: (res) => {
                if (res.status && res.data) {
                    this.dialogRef.close({
                        flag: true,
                        operation: this.isAdd ? 'Added' : 'Updated',
                        data: res.data
                    });
                }
            },
            error: (err: any) =>  {
            },
            complete: () => {
            }
        });
    }

    ngOnInit() {
        if (this.driverEdit) {
            this.isAdd = false;
            this.driverForm.addControl('driverId', new FormControl('', Validators.required));
            this.driverForm.patchValue(this.driverEdit);
            this.driverForm.get('driverId').setValue(this.driverEdit._id);
        }
    }

}
