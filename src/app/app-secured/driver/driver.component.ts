import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AppCreateEditDriverComponent } from './create-edit-driver/create-edit-driver.component';
import { AppProviderService } from '../common/service/provider.service';
import { BehaviorSubject } from 'rxjs';
import { IDriverList } from '../common/service/provider';
import { AppalertComponent } from '../common/alert/alert.component';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class AppDriverComponent implements OnInit {
  driverList$ = new BehaviorSubject<IDriverList[]>([]);
  displayedColumns: string[] = ['id', 'name', 'phoneNo', 'address', 'createdOn', 'action'];
  constructor(public dialog: MatDialog, private snackBar: MatSnackBar,
              private appProviderService: AppProviderService) { }

  ngOnInit() {
    this.appProviderService.getDriverList().subscribe({
      next: (res) => {
          if (res.status && res.data) {
            this.driverList$.next(res.data);
          }
      },
      error: (err: any) =>  {
      },
      complete: () => {
      }
    });
  }

  addDriver(driverEdit?: IDriverList): void {
    const dialogRef = this.dialog.open(AppCreateEditDriverComponent, {
      width: '500px',
      data: driverEdit ? driverEdit : null
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.flag) {
        this.openSnackBar(`Driver ${result.operation} Successfully`, 'CLOSE');
        this.driverList$.next(result.data);
      }
    });
  }

  editDriver(item): void {
    this.addDriver(item);
  }

  deleteDriver(payload) {
    this.appProviderService.removeDriver(payload).subscribe({
      next: (res) => {
        if (res.status && res.data) {
          this.openSnackBar(`Driver Deleted Successfully`, 'CLOSE');
          this.driverList$.next(res.data);
        }
      },
      error: (err: any) =>  {
      },
      complete: () => {
      }
    });
  }

  confirmationDialog(item): void {
    const confirmDialog = this.dialog.open(AppalertComponent, {
      disableClose: true,
      data: `Are you want to delete ${item.driverName}`
    });
    const payload =  {driverId: item._id};
    confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteDriver(payload);
      }
    });
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      horizontalPosition: 'end',
      verticalPosition: 'bottom',
      duration: 2000
    });

    snackBarRef.onAction().subscribe(() => {
    });
  }
}
