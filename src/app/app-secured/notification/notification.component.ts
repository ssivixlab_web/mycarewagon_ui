import { Component, OnInit } from '@angular/core';
import { AppProviderService } from '../common/service/provider.service';
import { INotificationList, INotification } from '../common/service/provider';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class AppNotificationComponent implements OnInit {
  notificationList$ = new BehaviorSubject<INotification[]>([]);
  page: number = 1;
  loadmoreflag: boolean = false;
  constructor(private appService: AppProviderService) { }

  ngOnInit() {
    this.getNotificationList();
  }

  getNotificationList() {
    this.appService.getNotificationList({page: this.page}).subscribe((res: INotificationList) => {
      if (res.status && res.data.length > 0) {
        this.update(res.data);
      }
    });
  }

  update(moreData: Array<INotification>) {
    if (moreData.length < 10) {
      this.loadmoreflag = true;
    }
    const currentValue = this.notificationList$.value;
    const updatedValue = currentValue.concat(...moreData);
    this.notificationList$.next(updatedValue);
  }

  loadMore() {
    this.page = this.page + 1;
    this.getNotificationList();
  }

  getTime(sendOn): string {
    return moment(sendOn).format('hh:mm A') + ' | ' + moment(sendOn).format('MMM DD, YYYY');
  }

}
