import { Component, OnInit } from '@angular/core';
import { AppService } from '../appService/appService';
import { Router } from '@angular/router';
import { AppProviderService } from '../service/provider.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class AppNavbarComponent implements OnInit {

  constructor(private appService: AppService,
              private router: Router,
              private appProvSer: AppProviderService) { }
  isCollapsed = true;
  ngOnInit() {
  }

  toggleSidebarPin() {
    this.appService.toggleSidebarPin();
  }
  toggleSidebar() {
    this.appService.toggleSidebar();
  }

  logout() {
    this.appProvSer.removeSession().subscribe((res: boolean) => {
      if (res) {
        this.router.navigateByUrl('');
      }
    });
  }

}
