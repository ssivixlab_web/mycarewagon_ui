import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material';
import { IPushNotificationResponse } from '../service/provider';

@Component({
    selector: 'app-push-notification',
    templateUrl: 'pushNotification.component.html'
})
export class AppPushNotificationComponent implements OnInit {
    constructor(public snackBarRef: MatSnackBarRef<AppPushNotificationComponent>,
                @Inject(MAT_SNACK_BAR_DATA) private pushNotifiData: IPushNotificationResponse) { }

    notificationRes: IPushNotificationResponse;

    ngOnInit() {
        if (this.pushNotifiData !== null ) {
          this.notificationRes = this.pushNotifiData;
        }
    }

    btnClick(result: boolean) {
      if (result) {
        this.snackBarRef.dismissWithAction();
      } else {
        this.snackBarRef.dismiss();
      }
    }

}
