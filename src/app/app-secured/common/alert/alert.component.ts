import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-alert.component',
    templateUrl: 'alert.component.html'
})
export class AppalertComponent {

    constructor(public dialogRef: MatDialogRef<AppalertComponent>,
                @Inject(MAT_DIALOG_DATA) public item: string) {
    }

    close(isDel?: boolean): void {
      this.dialogRef.close(isDel);
    }

}
