import { Injectable } from '@angular/core';
import { AppHttpApiService } from 'src/app/app-common/utility/http-api.service';
import { API_URL, OPERATION_TYPE } from 'src/app/app-common/contants';
import { Observable } from 'rxjs';
import { IBookingHistory, IDriverPayload, IDriverListResponse, IVehicleListResponse,
    IVehiclePayload, InvoiceResponse, INotificationList, IProfileUpdateResponse,
    IScheduleTrip, IUserDashBoard, IAcceptBookingRes } from './provider';
import { AppAuthenticationService } from 'src/app/app-common/utility/authentication.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class AppProviderService {
    constructor(private httpApi: AppHttpApiService) {
    }

    getUserIDHeader() {
        const userId = AppAuthenticationService.getUserInfo()._id;
        const HttpUploadOptions = {
            headers: new HttpHeaders({
                userId: `${userId}`
            })
        };
        return HttpUploadOptions;
    }

    removeSession(): Observable<boolean> {
        return new Observable((observer) => {
            const url = `${API_URL.LOGOUT}?userId=${AppAuthenticationService.getUserInfo()._id}`;
            let isVerified: boolean = false;
            this.httpApi.getData(url)
            .subscribe({
                next: (res) => {
                    if (res && res.status) {
                        AppAuthenticationService.removeSessionStorage();
                        isVerified = true;
                    } else {
                        isVerified = false;
                    }
                },
                error: (err: any) =>  {
                    observer.error(err.error);
                    isVerified = false;
                },
                complete: () => {
                    observer.next(isVerified);
                    observer.complete();
                }
            });
        });
    }

    getCompletedTripList(page): Observable<IBookingHistory> {
        return this.httpApi.postData(API_URL.COMPLETED_TRIP, page);
    }

    driverManagement(operation: string, driverDetails: IDriverPayload): Observable<IDriverListResponse> {
        let url: string;
        if (operation === OPERATION_TYPE.ADD) {
            url = API_URL.ADD_DRIVER;
        } else if (operation === OPERATION_TYPE.EDIT) {
            url = API_URL.UPDATE_DRIVER;
        }
        return this.httpApi.postData(url, driverDetails, this.getUserIDHeader().headers);
    }

    getDriverList(): Observable<IDriverListResponse>  {
        return this.httpApi.postData(API_URL.DRIVER_LIST, {});
    }

    removeDriver(payload) {
        return this.httpApi.postData(API_URL.REMOVE_DRIVER, payload);
    }

    getVehicleList(): Observable<IVehicleListResponse>  {
        return this.httpApi.postData(API_URL.VEHICLE_LIST, {});
    }

    removeVehicle(payload) {
        return this.httpApi.postData(API_URL.REMOVE_VEHICLE, payload);
    }

    vehicleManagement(operation: string, vehicleDetails: IVehiclePayload): Observable<IVehicleListResponse> {
        let url: string;
        if (operation === OPERATION_TYPE.ADD) {
            url = API_URL.ADD_VEHICLE;
        } else if (operation === OPERATION_TYPE.EDIT) {
            url = '';
        }
        return this.httpApi.postData(url, vehicleDetails, this.getUserIDHeader().headers);
    }

    getInvoice(payload): Observable<InvoiceResponse> {
        return this.httpApi.postData(API_URL.GET_INVOICE, payload);
    }

    getNotificationList(page): Observable<INotificationList> {
        return this.httpApi.postData(API_URL.GET_NOTIFICATION, page);
    }

    updateProfile(profile, photo): Observable<IProfileUpdateResponse> {
        const formData = new FormData();
        if (photo) {
           formData.append('image', photo, photo.name);
        }
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        formData.append('data', JSON.stringify(profile));
        return this.httpApi.postData(API_URL.UPDATE_PROFILE, formData, HttpUploadOptions.headers);
    }

    getScheduleTrip(): Observable<IScheduleTrip> {
        return this.httpApi.postData(API_URL.SCHEDULE_TRIP, {page: 1});
    }

    pickupTrip(payload): Observable<IScheduleTrip> {
        return this.httpApi.postData(API_URL.TRIP_PICKUP, payload);
    }
    tripCompleted(payload): Observable<IScheduleTrip> {
        return this.httpApi.postData(API_URL.TRIP_COMPLETED, payload);
    }

    getUserDashboard(): Observable<IUserDashBoard> {
        return this.httpApi.postData(API_URL.USER_DETAILS, {});
    }

    getDriverAndVehicleDetailsForBooking(payload): Observable<IAcceptBookingRes> {
        return this.httpApi.postData(API_URL.DRIVER_VEHICLE_LIST, payload);
    }

    acceptTrip(payload) {
        return this.httpApi.postData(API_URL.ACCEPT_TRIP, payload);
    }

}
