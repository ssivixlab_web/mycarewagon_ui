import { IUserData } from 'src/app/app-landing/service/landing';

// ENUM [key (DB) -- value (UI)]

export enum TRIP_TYPE {
    SINGLE = 'single',
    ROUND = 'round'
}

export enum PAYMENT_TYPE {
    CASH = 'cash',
    CARD = 'card'
}

export enum BOOKING_TYPE {
    BOOK_NOW = 'now',
    BOOK_LATER = 'later'
}

export enum DRIVER_STATUS {
    AVAILABLE = 'available',
    ENGAGED = 'engaged'
}

export enum VEHICLE_STATUS {
    AVAILABLE = 'available',
    ENGAGED = 'engaged'
}

export enum PAYMENT_STATUS {
    PENDING = 'pending'
}

export enum USER_STATUS {
    REGISTERED = 'registered',
    NOT_REGISTERED = 'not_registered'
}

export enum ACTIVE {
    ENABLED = 'enabled',
    DISABLED = 'disabled'
}

export enum BOOKING_STATUS {
    REQUESTED = 'requested',
    SCHEDULED = 'scheduled',
    ON_GOING = 'ongoing',
    COMPLETED = 'completed',
    NO_RESPONSE = 'noresponse',
    CANCELLED = 'cancel'
}

export enum GENDER {
    MALE = 'm',
    FEMALE = 'f'
}

// --------------------------------------------------------------------------

export interface Idriver {
    driverName: string;
    phoneNumber: string;
    countryCode: string;
    identificationNumber: string;
    address: string;
    createdOn: Date;
    _id: string;
    providerRefId: string;
    driverStatus: DRIVER_STATUS;
    isEnabled: ACTIVE;
    trip: ITrip[];
}

export interface IVehicle {
    vehicleNumber: string;
    vehicleType: string;
    createdOn: Date;
    _id: string;
    providerRefId: string;
    vehicleStatus: VEHICLE_STATUS;
    isEnabled: ACTIVE;
    trip: ITrip[];
}

export interface ITrip {
    tripTime: Date;
    bookingType: BOOKING_TYPE;
    tripId: string;
    _id: string;
}

export interface IAdditionalInfo {
    itemName: string;
    itemAmount: string;
    isItemUsed: boolean;
    _id: string;
}

export interface IUserRelative {
    createdOn: Date;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    NRIC: string;
    profilePic: string;
    gender: GENDER;
    spouse: string;
    insuranceProvider: string;
    insuranceNumber: string;
}

export interface IAmendmentAmount {
    defaultCharge: string;
    extraCharge: string;
}

export interface IUser {
    phoneNumber: string;
    profilePic: string;
    countryCode: string;
    mobileNumberVerified: boolean;
    createdOn: Date;
    userStatus: USER_STATUS;
    relativeDetails: IUserRelative[];
    maximumDistance: string;
    _id: string;
}

export interface IAmbulanceBooking {
    amendmentAmount: IAmendmentAmount;
    vehicleType: string;
    tripType: TRIP_TYPE;
    pickupLocation: string;
    dropLocation: string;
    unitNumber: string|null;
    fare: string;
    specialNotes: string|null;
    paymentMethod: PAYMENT_TYPE;
    bookingType: BOOKING_TYPE;
    bookingTime: Date;
    requestedBy: IUser;
    requestedTime: Date | string;
    acceptedBy: string;
    acceptedTime: Date;
    driverId: Idriver;
    vehicleId: IVehicle;
    bookingStatus: BOOKING_STATUS;
    rejectedTime: Date;
    pickedUpTime: Date;
    returnTime: Date;
    returnNotSure: boolean;
    completedTime: Date;
    cancelTime: Date;
    additionalItem: IAdditionalInfo[];
    invoiceNumber: string;
    country: string;
    city: string;
    paymentStatus: PAYMENT_STATUS;
    orderId: string;
    transactionId: string;
    refundStatus: string;
    tripAmount: string;
    paymentId: string;
    _id: string;
}

// ====================================================================

export interface IBookingHistory {
    status: boolean;
    data: IAmbulanceBooking[];
}

export interface IDriverPayload {
    driverName: string;
    countryCode: string;
    phoneNumber: string;
    address: string;
    driverId?: string;
}

export interface IDriverListResponse {
    status: boolean;
    data: IDriverList[];
}

export interface IDriverList {
    driverName: string;
    phoneNumber: string;
    countryCode: string;
    identificationNumber: string|null;
    address: string;
    createdOn: string;
    driverStatus: string;
    isEnabled: string;
    trip: [];
    _id: string;
    providerRefId: string;
}

export interface IVehicleListResponse {
    status: boolean;
    data: IVehicle[];
}

export interface IVehiclePayload {
    vehicleNumber: string;
    vehicleType: string;
}

export interface InvoiceResponse {
    status: boolean;
    data: InvoiceMsg;
}

export interface InvoiceMsg {
    status: boolean;
    data: string;
}

export interface INotificationList {
    status: boolean;
    data: INotification[];
}

export interface INotification {
    to: string;
    title: string;
    notify_about: string;
    referenceId: string;
    referenceCollection: string;
    sendOn: string;
    isRead: string;
    readOn: null;
    message: INotMsg;
    _id: string;
}

export interface INotMsg {
    trip: IAmbulanceBooking;
    user: IUser;
}

export interface IProfileUpdateResponse {
    data: IUserData;
    status: boolean;
}

export interface IScheduleTrip {
    status: boolean;
    data: IAmbulanceBooking[];
}

export interface IUserDashBoard {
    status: boolean;
    data: any;
}

export interface IPushNotificationData {
    'gcm.notification.lights': string;
    'gcm.notification.date': string;
    'gcm.notification.vibrate': string;
    'notify_about': string;
    'message': string;
    'body': string;
}

export interface IPushNotificationResponse {
    data: IPushNotificationData;
    from: string;
    notification: {
      title: string;
      body: string;
      icon: string;
    };
    collapse_key: string;
}

export interface IAcceptBookingData {
    driver: Idriver[];
    vehicle: IVehicle[];
}

export interface IAcceptBookingRes {
    status: boolean;
    data: IAcceptBookingData;
}
