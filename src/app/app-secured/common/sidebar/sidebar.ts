// Sidebar nav
export interface ISideNav {
    path: string;
    title: string;
    icon: string;
    class?: string;
    extralink?: boolean;
  }
