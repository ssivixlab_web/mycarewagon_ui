import { Injectable } from '@angular/core';
import { ISideNav } from './sidebar';

const MENUITEMS = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard' },
    { path: 'history', title: 'History', icon: 'history' },
    { path: 'driver', title: 'Manage Driver', icon: 'group_add' },
    { path: 'vehicle', title: 'Manage Vehicle', icon: 'directions_car' },
    { path: 'notification', title: 'Notification', icon: 'notifications' },
    { path: 'request', title: 'Schedule Request', icon: 'schedule' },
    { path: 'policy', title: 'Privacy Policy', icon: 'policy' },
    { path: 'terms', title: 'Terms And Conditions', icon: 'ballot' }
  ];

@Injectable()
export class AppSidebarService {
  getMenuitem(): ISideNav[] {
    return MENUITEMS;
  }
}
