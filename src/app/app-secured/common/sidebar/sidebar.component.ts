import { Component, OnInit } from '@angular/core';
import { AppSidebarService } from './sidebar.service';
import { ISideNav } from './sidebar';
import { AppAuthenticationService } from 'src/app/app-common/utility/authentication.service';
import { AppPublishSubscribeService } from 'src/app/app-common/pub-sub/publish-subscribe.service';
import { PUBS_CONSTANT } from 'src/app/app-common/contants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class AppSidebarComponent implements OnInit {
  sideNav: ISideNav[];
  userInfo: any;
  constructor(private appSideNav: AppSidebarService,
              private pubs: AppPublishSubscribeService) { }

  ngOnInit() {
    this.sideNav = this.appSideNav.getMenuitem();
    this.userInfo = AppAuthenticationService.getUserInfo();
    this.pubs.subscribeEvData(PUBS_CONSTANT.UPDATE_PROFILE_UI_VIEW, () => {
      this.userInfo = AppAuthenticationService.getUserInfo();
    });
  }

}
