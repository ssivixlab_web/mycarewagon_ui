import { Component, Inject, OnInit } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TRIP_TYPE, IPushNotificationResponse, INotMsg,
    PAYMENT_TYPE, IAcceptBookingRes, Idriver, IVehicle } from '../common/service/provider';
import * as moment from 'moment';
import { AppProviderService } from '../common/service/provider.service';

@Component({
    selector: 'app-accept-request.component',
    templateUrl: 'accept-request.component.html',
    styleUrls: ['./accept-request.component.scss']
})
export class AppacceptRequestComponent implements OnInit {
    changeEdit: FormGroup;
    uiData: any = {};
    driverList: Idriver[];
    veichleList: IVehicle[];
    private pushnotificationData: INotMsg = {} as INotMsg;

    constructor(private fb: FormBuilder,
                private appProviderService: AppProviderService,
                public dialogRef: MatDialogRef<AppacceptRequestComponent>,
                @Inject(MAT_DIALOG_DATA) public pushNotifiRes: IPushNotificationResponse) {
        this.changeEdit = this.fb.group({
            driverId: new FormControl('', [Validators.required]),
            vehicleId: new FormControl('', [Validators.required]),
            bookingId: new FormControl('', [Validators.required]),
            customerId: new FormControl('', [Validators.required]),
            tripType: new FormControl('', [Validators.required])
        });
        this.pushnotificationData = JSON.parse(this.pushNotifiRes.data.body);
    }

    ngOnInit(): void {
       setTimeout(() => {
        this.getDriverAndVehicleDetails();
       });
       this.prepareuiData();
    }

    prepareuiData() {
        this.uiData = {
            requestByUserName: this.pushnotificationData.user.relativeDetails &&
                this.pushnotificationData.user.relativeDetails.length !== 0 ?
                (this.pushnotificationData.user.relativeDetails[0].firstName)
                .concat(' ' + this.pushnotificationData.user.relativeDetails[0].lastName)
                : this.pushnotificationData.user.phoneNumber,
            requestTime:  moment(this.pushnotificationData.trip.requestedTime).format('MMM Do, YYYY h:mm A'),
            requestedUnit: this.pushnotificationData.trip.unitNumber === '' ? 'NA' : this.pushnotificationData.trip.unitNumber,
            pickupLocation: this.pushnotificationData.trip.pickupLocation,
            dropLocation: this.pushnotificationData.trip.dropLocation,
            tripType: this.pushnotificationData.trip.tripType  === TRIP_TYPE.SINGLE ? TRIP_TYPE.SINGLE : TRIP_TYPE.ROUND,
            returnTime: this.pushnotificationData.trip.requestedTime ?
                    (this.pushnotificationData.trip.requestedTime === 'NOT SURE') ? 'Not Sure' :
                    moment(this.pushnotificationData.trip.requestedTime).format('MMM Do, YYYY hh:mm A') : 'N/A',
            additionalItem: this.pushnotificationData.trip.additionalItem,
            paymentMethod: this.pushnotificationData.trip.paymentMethod === PAYMENT_TYPE.CARD ? PAYMENT_TYPE.CARD : PAYMENT_TYPE.CASH
        };
        const formData = {
            driverId: null,
            vehicleId: null,
            bookingId: this.pushnotificationData.trip._id,
            customerId: this.pushnotificationData.user._id,
            tripType: this.pushnotificationData.trip.tripType,
        };
        this.changeEdit.patchValue(formData);
    }

    getDriverAndVehicleDetails() {
        const requestData = {
            bookingId: this.pushnotificationData.trip._id,
            vehicleType: this.pushnotificationData.trip.vehicleType
        };
        this.appProviderService.getDriverAndVehicleDetailsForBooking(requestData)
        .subscribe((driverAndVehicleDetails: IAcceptBookingRes) => {
            if (driverAndVehicleDetails.data && driverAndVehicleDetails.status) {
                this.driverList = driverAndVehicleDetails.data.driver;
                this.veichleList = driverAndVehicleDetails.data.vehicle;
            }
        });
    }

    acceptTrip(payload) {
        this.appProviderService.acceptTrip(payload).subscribe((res) => {
            this.dialogRef.close();
        });
    }

    close(): void {
        this.dialogRef.close();
    }

}
