import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AppProviderService } from '../common/service/provider.service';
import { AppAuthenticationService } from 'src/app/app-common/utility/authentication.service';
import { IProfileUpdateResponse } from '../common/service/provider';
import { SESSION_STORAGE_KEY, PUBS_CONSTANT } from 'src/app/app-common/contants';
import { AppPublishSubscribeService } from 'src/app/app-common/pub-sub/publish-subscribe.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class AppProfileComponent implements OnInit {
  profileForm: FormGroup;
  userInfo: any;
  imgURL: any;
  files: any;
  errors: string = '';
  constructor(private fb: FormBuilder,
              private appProviderService: AppProviderService,
              private pubs: AppPublishSubscribeService) {
    this.profileForm = this.fb.group({
      companyName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
      country: new FormControl('Singapore', Validators.required),
      address: new FormControl('', Validators.required),
    });
   }

  ngOnInit() {
    this.userInfo = AppAuthenticationService.getUserInfo();
    if (this.userInfo.companyAddress) {
      this.userInfo.address = this.userInfo.companyAddress;
      delete this.userInfo.companyAddress;
    }
    this.profileForm.patchValue(this.userInfo);
    if ( this.userInfo.logo != null ) {
      this.imgURL = this.userInfo.logo;
    }
  }

  onFileChanged(event: any) {
    this.files = event.target.files[0];

    if (this.files.length === 0) {
      return;
    }

    const mimeType = this.files.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.files);
    reader.onload = () => {
      this.imgURL = reader.result;
    };
  }

  removeImage() {
    this.imgURL = null;
    this.files = null;
  }

  submit(payload) {
    this.errors = '';
    this.appProviderService.updateProfile(payload , this.files).subscribe({
      next: (res: IProfileUpdateResponse) => {
        if (res.status && res.data) {
          // Update the data in LocalStorage and publish the event which will update the UI
          AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(res.data));
          this.pubs.publishEvData(PUBS_CONSTANT.UPDATE_PROFILE_UI_VIEW);
        }
      },
      error: (err: any) =>  {
        this.errors = err.error_message;
      },
      complete: () => {

      }
    });
  }

}
