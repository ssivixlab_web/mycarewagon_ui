import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { VEHICLE, OPERATION_TYPE } from 'src/app/app-common/contants';
import { IVehicle } from '../../common/service/provider';
import { AppProviderService } from '../../common/service/provider.service';

@Component({
    selector: 'app-create-edit-vehicle',
    templateUrl: './create-edit-vehicle.component.html',
    styleUrls: ['./create-edit-vehicle.component.scss']
  })
  export class AppCreateEditVehicleComponent implements OnInit {
    vehicleForm: FormGroup;
    isAdd: boolean = true;
    vechiletypleList: any[];
    constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<AppCreateEditVehicleComponent>,
                private appProviderService: AppProviderService,
                @Inject(MAT_DIALOG_DATA) public vehicleEdit: IVehicle) {
        this.vehicleForm = this.fb.group({
          vehicleNumber: new FormControl('', Validators.required),
          vehicleType: new FormControl('', Validators.required)
        });
    }

    addVehicle(payload): void {
      const opType =  this.isAdd ? OPERATION_TYPE.ADD : OPERATION_TYPE.EDIT;
      this.appProviderService.vehicleManagement(opType, payload).subscribe({
        next: (res) => {
            if (res.status && res.data) {
                this.dialogRef.close({
                    flag: true,
                    operation: this.isAdd ? 'Added' : 'Updated',
                    data: res.data
                });
            }
        },
        error: (err: any) =>  {
        },
        complete: () => {
        }
      });
    }

    ngOnInit() {
      this.vechiletypleList = VEHICLE;
      if (this.vehicleEdit) {
        this.isAdd = false;
        this.vehicleForm.patchValue(this.vehicleEdit);
      }
    }

}
