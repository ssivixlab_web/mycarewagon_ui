import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AppCreateEditVehicleComponent } from './create-edit-vehicle/create-edit-vehicle.component';
import { AppProviderService } from '../common/service/provider.service';
import { BehaviorSubject } from 'rxjs';
import { IVehicle } from '../common/service/provider';
import { AppalertComponent } from '../common/alert/alert.component';
import { VEHICLE } from 'src/app/app-common/contants';
import * as _ from 'lodash';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class AppVehicleComponent implements OnInit {
  displayedColumns: string[] = ['id', 'vechileno', 'vechileType', 'createdOn', 'action'];
  vechiletypleList: any[];
  vehicleList$ = new BehaviorSubject<IVehicle[]>([]);
  constructor(public dialog: MatDialog, private snackBar: MatSnackBar,
              private appProviderService: AppProviderService) { }

  ngOnInit() {
    this.vechiletypleList = VEHICLE;
    this.appProviderService.getVehicleList().subscribe({
      next: (res) => {
          if (res.status && res.data) {
            this.vehicleList$.next(res.data);
          }
      },
      error: (err: any) =>  {
      },
      complete: () => {
      }
    });
  }

  getVehicleName(vechileKey: string): string {
    return _.find(this.vechiletypleList, (o) => {
      return o.value === vechileKey;
    }).key;
  }

  addVehicle(vehicleEdit?: IVehicle): void {
    const dialogRef = this.dialog.open(AppCreateEditVehicleComponent, {
      width: '500px',
      data: vehicleEdit ? vehicleEdit : null
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.flag) {
        this.openSnackBar(`Vehicle ${result.operation} Successfully`, 'CLOSE');
        this.vehicleList$.next(result.data);
      }
    });
  }

  editVehicle(item): void {
    // this.addVehicle(item);
  }

  confirmationDialog(item): void {
    const confirmDialog = this.dialog.open(AppalertComponent, {
      disableClose: true,
      data: `Are you want to remove ${item.vehicleNumber} vehicle?`
    });
    const payload =  {vehicleId: item._id};
    confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.deleteVehicle(payload);
      }
    });
  }

  deleteVehicle(payload) {
    this.appProviderService.removeVehicle(payload).subscribe({
      next: (res) => {
        if (res.status && res.data) {
          this.openSnackBar(`Vehicle Deleted Successfully`, 'CLOSE');
          this.vehicleList$.next(res.data);
        }
      },
      error: (err: any) =>  {
      },
      complete: () => {
      }
    });
  }

  openSnackBar(message: string, action: string) {
    const snackBarRef = this.snackBar.open(message, action, {
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });

    snackBarRef.onAction().subscribe(() => {
    });
  }
}
