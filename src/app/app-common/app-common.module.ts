import { NgModule } from '@angular/core';
import { SafePipe } from './utility/safe.pipe';
import { AppTermsConditionComponent } from '../app-secured/terms-condition/terms-condition.component';
import { AppNumberOnlyDirective } from './utility/number-only.directive';

@NgModule({
  declarations: [SafePipe,
    AppTermsConditionComponent,
    AppNumberOnlyDirective
],
  imports: [],
  exports: [SafePipe, AppTermsConditionComponent, AppNumberOnlyDirective],
  providers: []
})
export class AppCommonModule { }
