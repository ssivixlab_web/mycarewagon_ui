import { Injectable } from '@angular/core';
import { SESSION_STORAGE_KEY } from '../contants';

@Injectable()
export class AppAuthenticationService {
    // check for session
    public static isAuthenticated(): boolean {
      const a = (sessionStorage[SESSION_STORAGE_KEY.TOKEN] && sessionStorage[SESSION_STORAGE_KEY.USER_DATA]) ? true : false;
      return a;
    }

    public static setSessionStorage( sessionKey: string, sessionValue: string) {
        sessionStorage[sessionKey] = sessionValue;
    }

    public static getUserInfo() {
        return JSON.parse(sessionStorage[SESSION_STORAGE_KEY.USER_DATA]);
    }

    public static getSessionId() {
        return sessionStorage[SESSION_STORAGE_KEY.TOKEN];
    }

    public static removeSessionStorage() {
        sessionStorage.removeItem(SESSION_STORAGE_KEY.TOKEN);
        sessionStorage.removeItem(SESSION_STORAGE_KEY.USER_DATA);
    }
}
