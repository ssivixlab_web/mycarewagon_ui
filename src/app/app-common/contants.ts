import { environment } from 'src/environments/environment';

export const API_END_POINT_URL = environment.endPointUrl;
// All get/post/put/delete End point
export const API_URL = {
    LOGIN: 'provider/login',
    REGISTER: 'provider/sign-up',
    OTP_VERIFY: 'provider/otp/verification',
    LOGOUT: 'provider/logout',
    COMPLETED_TRIP: 'provider/completed/trip',
    GET_INVOICE: 'provider/generate/invoice',
    GET_NOTIFICATION: 'provider/notification/list',
    UPDATE_PROFILE: 'provider/profile/update',
    // Driver Management
    DRIVER_LIST: 'provider/driver/list',
    ADD_DRIVER: 'add/driver',
    UPDATE_DRIVER: 'update/driver',
    REMOVE_DRIVER: 'remove/driver',
    // Vehicle Management
    VEHICLE_LIST: 'provider/vehicle/list',
    ADD_VEHICLE: 'add/vehicle',
    REMOVE_VEHICLE: 'remove/vehicle',
    SCHEDULE_TRIP : 'provider/scheduled/trip',
    TRIP_PICKUP : 'pickup/vehicle',
    TRIP_COMPLETED : 'trip/completed',
    USER_DETAILS : 'provider/detail',
    // Notification
    NOTIFICATION_ALLOW: 'provider/notification/allowed',
    ACCEPT_BOOKING: 'accept/vehicle/booking',
    DRIVER_VEHICLE_LIST: 'driver/vehicle/list',
    ACCEPT_TRIP: 'provider/update/trip',
    FORGOT_PASWORD: 'provider/forgot/password',
    RESET_PASWORD: 'provider/reset/password'
};

// Base/Configured data
export const STATIC_BASE_API_URL = {
    PRIVACY: 'https://ssivixlab.com/privacypolicy.htm',
    TERMSCONDITION: 'https://ssivixlab.com/termsandcondition.html'
};

// Application Constants

// Session, Header, Localstorage related constants
export const SESSION_STORAGE_KEY = {
    TOKEN : 'SSIVIXLAB_PROVIDER_authtoken',
    USER_DATA: 'SSIVIXLAB_PROVIDER_user_data'
};

export const VALIDATION = {
    MAX_LENGTH: 60,
    EMAIL_REGX: '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
};

export enum OPERATION_TYPE {
    ADD = 'ADD',
    EDIT = 'EDIT',
    DELETE = 'DELETE'
}

export const VEHICLE = [
    {key: 'Medical Transport', value: 'AMBULANCE'},
    {key: 'Other Vehicle', value: 'OTHER_VEHICLE'}
];

export const PUBS_CONSTANT = {
    UPDATE_PROFILE_UI_VIEW: 'UPDATE_PROFILE_UI_VIEW'
};
