import { Component, OnInit } from '@angular/core';
import { MessagingService } from './app-secured/common/appService/messaging.service';
import { IPushNotificationResponse } from './app-secured/common/service/provider';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit() {
  }
}
